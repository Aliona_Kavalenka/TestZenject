using UnityEngine;

public class Player : MonoBehaviour
{
    private float _movingSpeed = 5f;

    public void MovingUp() => transform.Translate(0, 0, _movingSpeed * Time.deltaTime);

    public void MovingDown() => transform.Translate(0, 0, -(_movingSpeed * Time.deltaTime));

    public void MovingLeft() => transform.Translate(-(_movingSpeed * Time.deltaTime), 0, 0);

    public void MovingRight() => transform.Translate(_movingSpeed * Time.deltaTime, 0, 0);
}
