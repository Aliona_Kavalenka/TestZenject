using UnityEngine;
using Zenject;

public class PlayerManager : ITickable
{ 
    [Inject] private Player _palyer;

    public void Tick()
    {
        Move();
    }

    private void Move()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            _palyer.MovingUp();
        }
        else if(Input.GetKey(KeyCode.DownArrow))
        {
            _palyer.MovingDown();
        }
        else if(Input.GetKey(KeyCode.RightArrow))
        {
            _palyer.MovingRight();
        }
        else if(Input.GetKey(KeyCode.LeftArrow))
        {
            _palyer.MovingLeft();
        }
    }
}
