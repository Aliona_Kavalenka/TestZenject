using Zenject;

public class GameInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<Player>().FromComponentInHierarchy(true).AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerManager>().AsSingle();
    }
}